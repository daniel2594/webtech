 'use strict'
 const express = require('express')
 const router = express.Router()
 const Sequelize = require('sequelize')
 const Op = Sequelize.Op


 const sequelize = new Sequelize(process.env.DB, process.env.DB_USER,
    process.env.DB_PASSWORD, {
        dialect : 'mysql',
        define : {
            timestamps : false
        }
    })

const Book = sequelize.import('../models/book-model')

router.post ('/sync', async (req, res) => {
	try{
		await sequelize.sync({force : true})
		res.status(201).json({message : 'created'})
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

router.get('/books', async (req, res) => {
	try{
	    let books
	    if (req.query && req.query.filter){
	        books = await Book.findAll({
	            where : {
	                name : {
	                   [Op.like] : `%${req.query.filter}%` 
	                }
	            }
	        })   
	    }
	    else{
	        books = await Book.findAll()    
	    }
		res.status(200).json(books)
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})


router.post('/books', async (req, res) => {
	try{
		if (req.query.bulk && req.query.bulk == 'on'){
			await Book.bulkCreate(req.body)
			res.status(201).json({message : 'created'})
		}
		else{
			await Book.create(req.body)
			res.status(201).json({message : 'created'})
		}
	}
	catch(e){
		console.warn(e.stack)
		res.status(500).json({message : 'server error'})
	}
})

router.get('/books/:id', async (req, res) => {
	try{
		let book = await models.Book.findByPk(req.params.id)
		if (book){
			res.status(200).json(book)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

router.put('/books/:id', async (req, res) => {
	try{
		let book = await Book.findByPk(req.params.id)
		if (book){
			await book.update(req.body)
			res.status(202).json({message : 'OK'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

router.delete('/books/:id', async (req, res) => {
	try{
		let book = await models.Book.findByPk(req.params.id)
		if (book){
			await Book.destroy()
			res.status(202).json({message : 'OK'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

 module.exports = router; 
 