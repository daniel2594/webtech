const SERVER = 'http://localhost:8080'
        window.onload = () => {

            let filter = ''

            let filterField = document.getElementById('filter')

            filterField.onchange = (evt) => {
				filter = evt.target.value
				loadAll(filter)
			}

            loadAll(filter)

            async function loadAll(filter){
                try{
                    let url = filter ? `${SERVER}/book-api/books?filter=${filter}` : `${SERVER}/book-api/books`
                    let response = await fetch(url)
                    let data = await response.json()
                    let table = document.getElementById('bookTable')
                    table.innerHTML = ''
                    for (let e of data){
                        let rowContent = `
                            <td>
                                <span class=view>
                                    ${e.name}
                                </span>
                                <span class="edit name">
									<input type=text value=${e.name} />
								</span>
                            </td>
                            <td>
                                <span class=view>
                                    ${e.author}
                                </span> 
                                <span class="edit author">
									<input type=text value=${e.author} />
								</span>
                            </td>
                            <td>
                                <span class=view>
                                    ${e.quote}
                                </span>
                                <span class="edit quote">
									<input type=text value=${e.quote} />
								</span>
                            </td>
                            <td>
								<span class=view>
									<input type=button value=edit />
									<input type=button value=delete />
								</span>
								<span class=edit>
									<input type=button value=save />
									<input type=button value=cancel />
								</span>
							</td>			
                        `
                        let row = document.createElement('tr')
                        row.innerHTML = rowContent
                        row.dataset.id = e.id
                        table.append(row)
                        document.querySelectorAll('.edit').forEach((e) => {
							e.style.display = 'none'
                        })
                        
                        let deleteButton = document.querySelector(`table tr[data-id="${e.id}"] input[value=delete]`)

						deleteButton.onclick = () => {
							deleteRow(e.id)
                        }
                        
                        let editButton = document.querySelector(`table tr[data-id="${e.id}"] input[value=edit]`)
                        editButton.onclick = (evt) => {
                            let row = evt.target.parentNode.parentNode.parentNode
                            row.querySelectorAll('.view').forEach((e) => {
                                e.style.display = 'none'
                            })
                            row.querySelectorAll('.edit').forEach((e) => {
                                e.style.display = 'inline-block'
                            })
                        }

                        let cancelButton = document.querySelector(`table tr[data-id="${e.id}"] input[value=cancel]`)
						cancelButton.onclick = (evt) => {
							let row = evt.target.parentNode.parentNode.parentNode

							row.querySelectorAll('.edit').forEach((e) => {
								e.style.display = 'none'
							})
							row.querySelectorAll('.view').forEach((e) => {
								e.style.display = 'inline-block'
							})
                        }
                        
                        let saveButton = document.querySelector(`table tr[data-id="${e.id}"] input[value=save]`)
						saveButton.onclick = (evt) => {
							let row = evt.target.parentNode.parentNode.parentNode
							let payload = {
								name : row.querySelector('.name input').value,
								address : row.querySelector('.author input').value,
								email : row.querySelector('.quote input').value,
								
							}
							saveRow(e.id, payload)
						}

                    }
                }
                catch(err){
                    console.warn(err)
                }
            }

            document.getElementById('addButton').onclick = async () => {
                let payload = {
                    name : document.getElementById('bookName').value,
                    address : document.getElementById('bookAuthor').value,
                    email : document.getElementById('bookQuote').value,
                
                }
                try{
                    await fetch(`${SERVER}/book-api/books`, {
                        method : 'post',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body : JSON.stringify(payload)
                    })
                    loadAll()
                }
                catch(err){
                    console.warn(err)
                }	
            }

            async function deleteRow(id){
				try{
					await fetch(`${SERVER}/books-api/books/${id}`, {
						method : 'delete'
					})
					loadAll(filter)
				}
				catch(err){
					console.warn(err)
				}	
			}

			async function saveRow(id, payload){
				try{
					await fetch(`${SERVER}/book-api/books/${id}`, {
						method : 'put',
						headers: {
							'Content-Type': 'application/json'
						},
						body : JSON.stringify(payload)
					})
					loadAll(filter)
				}
				catch(err){
					console.warn(err)
				}	
			}
        }