'use strict'
require('dotenv').config({silent:true })

const express = require('express')
const bodyParser = require('body-parser')

const bookRouter = require('./routers/book-router')

const  app = express()
app.unsubscribe(bodyParser.json())
app.use(express.static('static'))

app.use('/book-api', bookRouter)

app.listen(8080)