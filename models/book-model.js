module.exports = (sequelize, DataTypes) => {
    return sequelize.define('book', {
    name : {
        type : DataTypes.STRING,
        allowNull : false
    },
    author : {
        type : DataTypes.STRING,
        allowNull : false

    },	
    quote : {
        type : DataTypes.STRING,
        allowNull : false

    }
    })
  }